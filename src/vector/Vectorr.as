package vector 
{
	import flash.geom.Point;
	/**
	 * ...
	 * @author Roy Springer
	 */
	public class Vectorr {
		
		
		private var _x:Number;
		public function get x():Number { return _x; }
		
		private var _y:Number;
		public function get y():Number { return _y; }
		
		private var _length:Number;
		private var _radsToDeg:Number = Math.PI / 180;
		
		public function Vectorr(x:Number = 0, y:Number = 0) {
			this._x = x;
			this._y = y;
		}
		
		public function reset( x:Number = 0, y:Number = 0 ):void { _x = x; y = _y ; }
		public function clone():Vectorr { return new Vectorr(_x, _y); }

        public function add( pos:Vectorr ):void { _x += pos.x; _y += pos.y; }
        public function addXY( x:Number, y:Number ):void { _x += x; _y += y; }
        public function addPoint( point:Point ):void { _x += point.x; _y += point.y; }
 
        public function subtract( pos:Vectorr ):void { _x -= pos.x; _y -= pos.y; }
        public function subtractXY( x:Number, y:Number ):void { _x -= x; _y -= y; }
        public function subtractPoint( point:Point ):void { _x -= point.x; _y -= point.y; }
 
        public function muliply( vec:Vectorr ):void { _x *= vec.x; _y *= vec.y;}
        public function muliplyXY( x:Number, y:Number ):void { _x *= x; _y *= y; }
        public function muliplyPoint( point:Point ):void { _x *= point.x; _y *= point.y; }
 
        public function divide( vec:Vectorr ):void { _x /= vec.x; _y /= vec.y; }
        public function divideXY( x:Number, y:Number ):void { _x /= x; _y /= y; }
        public function dividePoint( point:Point ):void { _x /= point.x; _y /= point.y; }
		
        public function scale( s:Number, output:Vectorr = null ):Vectorr {
			if ( output ) {
				output.reset( _x * s, _y * s );
				return output;
			}else {
				_x *= s;
				_y *= s;
			}
			return this;
		}
		
        public function normalize( output:Vectorr = null ):Vectorr {
            const nf:Number = 1 / Math.sqrt(_x * _x + _y * _y);
			if ( output ) {
				output.reset( _x * nf, _y * nf );
				return output;
			}else {
				_x *= nf;
				_y *= nf;
			}
			return this;
        }
		
		/** Dot product **/
        public function dot(vec:Vectorr):Number { return _x * vec.x + _y * vec.y; }
        public function dotXY(x:Number, y:Number):Number { return _x * x + _y * y; }
 
        /** Cross determinant **/
        public function crossDet(vec:Vectorr):Number { return _x * vec.y - _y * vec.x; }
        public function crossDetXY(x:Number, y:Number):Number { return _x * y - _y * x; }
		
		/** Projection **/
		public function projectOn( other:Vectorr ):Vectorr {
			var dp:Number = this.dot( other );
			return new Vectorr( dp * other.x, dp * other.y );
		}
		
        /** Rotate **/
        public function rotate(rads:Number, output:Vectorr = null):Vectorr {
			var currentLenght:Number = this.length;
			if ( output ) {
				output.reset( currentLenght * Math.cos(rads), currentLenght * Math.sin(rads) );
				return output;
			}else {
				_x = currentLenght * Math.cos(rads);
				_y = currentLenght * Math.sin(rads);
			}
			return this;
        }
		
		public function set angle_r( rads:Number ):void {
			var currentLenght:Number = this.length;
			_x = currentLenght * Math.cos(rads);
			_y = currentLenght * Math.sin(rads);
        }
		public function get angle_r():Number { return Math.atan2(this._y, this._x) }
		
        public function normalRight():Vectorr { return new Vectorr(-_y, _x); }
        public function normalLeft():Vectorr { return new Vectorr(_y, -_x); }
        public function invert():void { _x *= -1; _y *= -1; }
		
		public function get length():Number { 
			_length = Math.sqrt(_x * _x + _y * _y);
			return _length;
		}
		
		public function set length( newLength:Number ):void { 
			var currentLenght:Number = this.length;
			_x = newLength * _x / currentLenght;
			_y = newLength * _y / currentLenght;
		}
		public function get lengthSqr():Number { return _x * _x + _y * _y; }
		
		public function equals(vec:Vectorr):Boolean { return _x == vec.x && _y == vec.y; }
        public function equalsXY(x:Number, y:Number):Boolean { return _x == x && _y == y; }
        public function isZero():Boolean { return _x == 0 && _y == 0; }
		public function getDegrees():Number { return getRads() * _radsToDeg; }
        public function getRads():Number { return Math.atan2(_y, _x); }
		
		public function pointToVec( point:Point, output:Vectorr = null ):Vectorr {
			if ( !output ) output = new Vectorr(point.x, point.y);
			else output.reset( point.x, point.y );
			return output;
		}
		
		public function toString():String {
			return "[ x: " + this._x + ", y: " + this._y + ", length: " + this.length + " ]";
		}
	}
}